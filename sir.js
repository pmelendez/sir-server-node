var net = require('net');
var message_handlers = require("./messages")  
var active_sockets = [];
var monk = require('monk')
var db = monk('localhost:27017/tudomino')

var WebSocketServer = require('ws').Server
wss = new WebSocketServer({port: 11278});

var premium_rooms = [];
var free_rooms = [];
var sir = {};

var sendSocket = function(response, socket)
{
    console.log("Sending response: %j", response);
    socket.write(response);	
}

var sendWebSocket = function(response,ws)
{
    console.log("Sending response: %j", response);
    ws.send(response);	
}

var handlerMessage = function (data, sendResponse, socket)
{
    console.log("Incoming data: %j", data);

	var objData = JSON.parse(data);

	console.log("Object: %j", objData);

	if(objData['Type'] != undefined){
		console.log("%j",message_handlers);
		console.log("Incoming data: " + objData['Type']);
		var instruction = "var response = message_handlers." + objData['Type'] + ".handler(objData, active_sockets, sir, sendResponse, socket)";
		eval(instruction);
		console.log("Instruction: " + instruction);
	}
	else {
		console.log("ERROR: Not handler found for: " + data )
		sendResponse( "{ Type: '" + objData['Type'] +"-res', Error: 'Invalid message' }" );
	}

};

function InitServers()
{
    sir.db = db;
    sir.premium_rooms = premium_rooms;
    sir.free_rooms = free_rooms;

    wss.on('connection', function(ws) { 
        ws.on('message', function(data) { 
            handlerMessage(data, sendWebSocket, ws);
        });    
    });

    var server = net.createServer(function (socket) {
        socket.on('error', function(err) {
            console.error('SOCKET ERROR:', err);
        });

        socket.on('end', function(){
            console.log("Ending connection")

        })

        socket.on('data', function (data)  { 
            handlerMessage(data, sendSocket, socket);
        });

        active_sockets.push(socket);

        console.log("Incoming connection: " + active_sockets.join(','));          
    });

    console.log("\x1b[32mSir server ready.\x1b[0m");
    server.listen(14095, '100.72.136.42'); // '191.238.147.246');
}

function OnFindRooms(e, docs)
{
    if(docs != undefined)
    {
        premium_rooms = docs;

        for(var idx =0; idx < docs.length; idx++)
        {
            var room = docs[idx];
            //console.log("%j", room);
            if(room.type == "general")
            {
                free_rooms.push(room);
            }
        }

        console.log("General rooms: \x1b[33m%j\x1b[0m", free_rooms.length);
        console.log("Premium rooms: \x1b[33m%j\x1b[0m", premium_rooms.length);
        InitServers();
    }
    else
    {
        console.log("Couldn't find the rooms");
    }
}

db.get('room').find({}, {}, OnFindRooms);
