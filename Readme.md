#Sir multi user server

Sir[1] is a socket server written on Node.js for online turn-based games. It uses MongoDB for data persistance and receives and sends JSON formatted messages.


[1] The name Sir is an tribute to a dear Venezuelan cuatrist called [Sir Augusto Ramírez](http://es.wikipedia.org/wiki/Sir_Augusto_Ram%C3%ADrez)