module.exports = {
    handler : function (data, active_sockets, sir, sendResponse, socket) {
        console.log("Handling message type login for data: %j", data);
        var db = sir.db;

        var OnFind = function (e, docs)
        {
            var obj = {Type: "login-res", res:false, raw:docs}

            if(docs != undefined)
            {
                console.log("OnFind (docs): %j", docs); 
                if(docs.length > 0)
                {
                    obj.res = true;    
                    if(docs[0].status == "general")
                    {
                        obj.rooms = sir.free_rooms;
                    }  
                    else
                    {
                        // PM@NOTE Shouldn't we need to check the date of subscription?
                        obj.rooms = sir.premium_rooms;
                    }
                }
            }
            else
            {
                console.log("Error: %j", e);
                obj.error = true;
                obj.errorMessage = "Error querying the db";
            }

            sendResponse( JSON.stringify(obj), socket);

        };

        var col = db.get('user')
            col.find({ $and: [{username: data.user}, {password: data.password}]}, {}, OnFind)

    }
}
